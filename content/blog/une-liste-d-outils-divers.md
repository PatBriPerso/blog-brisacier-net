+++
date = "2017-01-28T21:20:08+01:00"
title = "Une liste d'outils divers"
banner = "img/outils.jpg"
summary = "Les outils informatiques que j'utilise"
tags = ["outils"]
draft = true
+++

# Ma liste d'outils

Depuis le temps que j'utilise un ordinateur, j'ai utilisé des centaines d'outils, de logiciels, d'utilitaires.
Voici une liste de ceux que j'ai appréciés ou que j'utilise aujourd'hui ainsi qu'une liste de ceux que j'essaierai
un jour.

- Todoist : site web pour gérer ses listes de tâches. Je l'ai utilisé longtemps. Très simple et efficace. Maintenant
  j'utilise NirvanaHQ
- Notepad++ : mon éditeur de texte préféré
- Lastpass : pour stocker tous mes mots de passe
- VLC : lecteur multimedia
- Audacity : manipulation audio
- Paint.NET : retouche de photos
- Calibre : gestion de livres électroniques
- TeamViewer : prise de contrôle à distance
- CCleaner : nettoyage de Windows
- Driver Booster : recherche et mise à jour des drivers 
- VirtualBox : gestion de machines virtuelles
- CDBurnerXP : gravure de CD/DVD. Génération d'images ISO
- Malwarebytes Anti-Malware : anti-malware donc
- Mouse without Borders : un utilitaire pour contrôler jusqu'à 4 PC à partir d'un seul clavier et d'une seule souris
- Royal TS : gestion des connexions à distance (SSH, RDP, etc). Multi-onglets.
- mailnesia.com : email jetable
- yopmail.com : email jetable

A essayer :

- Microsoft Sway : une nouvelle façon de faire des présentations sur le web
- Adobe Spark : pour créer des animations sur le web et les partager
- Plumbago : pour les PC tablette avec stylet. Il permet de prendre des notes manuscrites
- DupDetector : retrouve vos photos dupliquées et permet de supprimer les doubles
- Kodi : gérer sa vidéothèque
- TopDox : gestion centralisée de service de stockage dans le Cloud (Dropbox, OneDrive, etc)
- Glary Utilities : utilitaire pour entretenir son PC
- VirusKeeper 2017 : un antivirus gratuit et Français
- SRWare : Chrome mais sans les envois d'information vers Google
- Driver Genius 16 : recherche et mise à jour de drivers
- Snappy : recherche et mise à jour de drivers
- Kaspersky Safe Kids : protection des enfants contre les menaces sur Internet
- witigo Parental Filter 2 : protection des enfants contre les menaces sur Internet
- www.cnil.fr/vos-droits/vos-traces : Quelles traces laissons-nous sur Internet ?
- ninite : installation et mise à jour des programmes installés
- easycron.com : tâches planifiées en mode SaaS
- GeekUninstaller : pour désinstaller les programmes récalcitrants
- Traffic Shaper XP : pour simuler un débit Internet réduit
