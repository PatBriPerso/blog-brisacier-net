+++
date = "2016-12-24T15:39:18+01:00"
title = "Ouverture de mon blog"
tags = ["ouverture"]
banner = "img/ouverture.jpg"
summary = "Pourquoi j'ouvre mon blog"
+++

# J'ouvre mon blog

Cela fait un moment que j'en avais envie.

Je suis un passionné de technologie et d'informatique en particulier. Je suis développeur également.
J'essaie beaucoup de technologies en rapport avec le développement d'applications au sens large.
Comme beaucoup, je rencontre des problèmes pour utiliser ces technologies et je cherche sur Internet pour trouver
des réponses.

Quand je ne les trouve pas facilement, je m'acharne à les trouver moi-même. Et, quand je trouve des réponses, des solutions,
j'ai très envie de les publier sur le Net pour en faire profiter ceux qui rencontreront les mêmes problèmes.

Ce sont donc mes essais, mes problèmes et les solutions que j'y ai apportées que je vais publier sur ce blog.
