# Cette repo contient le contenu du site blog.brisacier.net

Le site est généré par Hugo puis publié comme un site statique sur AWS.

# Hugo

<https://gohugo.io/about/>

L'exécutable de Hugo est installé dans le dossier PatBriPerso\Hugo\bin.
Les sites sont dans PatBriPerso\Hugo\Sites.

J'ai ajouté hugo.cmd qui appelle le hugo.exe du répertoire bin car je ne voulais pas ajouter Hugo\bin à mon PATH.

## Mettre à jour Hugo

Sur la page <https://github.com/gohugoio/hugo/releases>, télécharger le ZIP contenant la dernière version (se terminant par Windows-64bit.zip).
Décompresser l'archive et mettre le contenu dans Hugo\bin

Pour verifier la version d'Hugo :

`hugo version`

## Thème

Parmis les nombreux thèmes, j'ai choisi Universal Theme for Hugo : <https://github.com/devcows/hugo-universal-theme>

Ce thème est installé comme une sous-repo du site avec cette commande :

`git submodule add https://github.com/devcows/hugo-universal-theme.git themes/hugo-universal-theme`

Pour récupérer la dernière version du thème, aller dans le dossier themes\hugo-universal-theme puis :

`git pull`

## Configuration

Le site est configuré dans le fichier `config.toml`. Les paramètres possibles dépendent aussi du thème choisi.

Pour la configuration globale, voir : <https://gohugo.io/getting-started/configuration/#all-configuration-settings>

Pour la configuration spécifique du thème, voir : <https://github.com/devcows/hugo-universal-theme#configuration>

## Commentaires

J'ai relié le site à Disqus pour autoriser les commentaires en bas de chaque article.

Voir cette doc : <https://gohugo.io/content-management/comments/>

## Rédiger des articles

La gestion du contenu est décrite dans cette doc : <https://gohugo.io/content-management/>. Les articles se trouvent dans le dossier `content/blog`.

Mes articles sont rédigés en Markdown. Une rubrique d'entête au dessus de l'article (délimité par 2 lignes `+++`) permet de définir certains paramètres.

Par exemple :

```markdown
+++
date = "2016-12-24T15:39:18+01:00"
title = "Ouverture de mon blog"
tags = ["ouverture"]
banner = "img/ouverture.jpg"

+++
```

Les variables possibles sont listées ici : <https://gohugo.io/content-management/comments/>

Pour créer un nouvel article, utiliser la commande :

`hugo new blog/titre-de-mon-article.md`

Hugo crée le fichier et il le crée en se basant sur le modèle `archetypes/blog.md`. Voir la docs sur les archetypes (modèles) : <https://gohugo.io/content-management/archetypes/>.

# Déploiement

## Comment publier

Pour publier en dev :

`publish-dev.cmd`

Le site de dev est accessible à l'adresse  : <https://dev-blog.brisacier.net/>

Pour publier en prod :

`publish-production.cmd`

Le site de production est accessible à l'adresse  : <https://blog.brisacier.net/>

## Tests et expérimentations

J'avais fait un déploiement avec GitLab CI qui poussait le site sur un hébergement OVH pour le dev et sur un site statique sur Google Cloud pour la prod.
J'ai supprimé mes projets sur Google Cloud et, certainement, supprimé aussi l'hébergement de mon blog sans m'en rendre compte. Pour OVH, je voudrais éviter de payer un hébergement juste pour publier un site statique donc je vais le résilier.

J'ai aussi découvert récemment Serverless Framework qui permet de publier des applications sans avoir de serveurs. Cela utilise des fonctions comme AWS Lambda (mais aussi d'autres fournisseurs Cloud). Pour AWS, ce framework génère une stack CloudFormation pour déployer le code et on peut créer une partie de l'infrastructure avec un syntaxe quasi identique à CloudFormation.

Il y a aussi un composant dédié à la publication d'un site statique.

La doc de ce composant : <https://github.com/serverless-components/website>.

J'ai utilisé le modèle de la v1 : <https://github.com/serverless-components/website/tree/v1> pour écrire mon serverless.yml

### Premier test avec le composant Website

A la première publication, j'ai ce message d'erreur :

`Error: Domain brisacier.net was not found in your AWS account. Please purchase it from Route53 first then try again.`

Effectivement, mon domaine n'est pas chez AWS mais chez OVH.
Je supprime le domaine de mon serverless.yml et je deploie à nouveau.
Je n'ai plus d'erreur mais quand je remets le domaine, j'ai la même erreur à nouveau.

Je laisse tomber cette méthode...

### Deuxième test avec les composants CloudFormation

Ma deuxième tentative consiste à créer moi-même les différents éléments dans serverless.yml sans utiliser le composant Website.

Les articles qui m'ont inspirés :

- <https://www.serverlessops.io/blog/static-websites-on-aws-s3-with-serverless-framework>
- <https://shinesolutions.com/2019/08/15/deploying-a-static-web-application-with-serverless-and-codebuild/>
- <https://github.com/tobilg/serverless-aws-static-websites>
- <https://github.com/sjevs/cloudformation-s3-static-website-with-cloudfront-and-route-53>

Le premier lien déploie le site sur S3 et paramètre un DNS sur l'URL correspondante. Cela ne permet pas (je pense) de faire du HTTPS, je n'ai donc pas essayé de le faire comme ça.

Le deuxième est plus complet et doit me permettre de faire ce que je veux.

Le troisième lien contient des plugins qui permettent de copier les fichiers sur S3 et d'invalider CloudFront.

Le quatrième lien décrit une stack CloudFormation qui déploie un site statique mais en utilisant le fichier d'index par défaut pour les sous-dossiers.

La difficulté a été de faire fonctionner le site de façon à ce que chaque URL vers un dossier affiche le document index.html contenu dans ce dossier par défaut. Par exemple pour ce lien : <https://blog.brisacier.net/blog/ouverture-de-mon-blog/>.

Dans mes premiers essais, la distribution CloudFront accédait aux fichiers de S3 par l'API d'AWS. Dans ce cas, les fichiers par défaut (index.html) réglé côté S3 pour le site web statique ne sont pas pris en compte. On peut ajouter un réglage dans CloudFront pour un document par défaut mais cela n'est valable que pour la racine du site. Cela peut fonctionner si on publie un site statique du type SPA (Single Page Application) fait avec ReactJS ou VueJS mais pas avec un site HTML plus classique.

J'ai donc fini par comprendre que CloudFront devait aller chercher les fichiers sur S3 en utilisant URL du site statique de S3. Dans ce cas, les fichiers par défaut sont bien renvoyés.

Au final, Serverless Framework crée une entrée DNS dans le domaine géré par AWS (simulang-was.com) et j'ai configuré manuellement le DNS de brisacier.net qui est chez OVH pour pointer vers l'URL d'AWS :

- dev : dev-blog.brisacier.net CNAME blog-brisacier-net-dev.simulang-aws.com
- production : blog.brisacier.net CNAME blog-brisacier-net-production.simulang-aws.com

Cela me permet de détruire puis de recréer la stack sans avoir à configurer à nouveau le DNS d'OVH. Cela aurait été le cas si j'avais mis comme cible des CNAME directement l'URL de CloudFront.
